# UXQuark Simple Install

> Tüm işlemlere başlamadan önce **NodeJS**, **NPM** ve **Bower** kurulu olması gerekiyor.

### NodeJS Installation

Öncelikle NodeJS'in kurulu olup olmadığını

```sh
$ node -v
```
komutu ile kontrol edebilirsiniz. Herhangi bir version numarası alamadıysanız

```
https://nodejs.org/en/
```
adresinden download edebilirsiniz. Eğer nodeJS kurulu ise NPM kurulumundan devam edebilirsiniz.

### NPM Installation

```sh
$ sudo npm install npm -g
```

### Bower Installation

```sh
$ npm install -g bower
```

> Tüm kurulumları gerçekleştirdikten sonra, start verilecek projenin folder'ını oluşturmanız gerekmektedir. Daha sonra Bitbucket bağlantısını yaptıktan sonra oluşturduğunuz proje dosya içerisine isterseniz direk **zip** olarak isteseniz **git clone** ile  dosyaları çekebilirsiniz.


### UXQuark Simple Kurulumu

```sh
$ cd /path/folder
```
ile start vereceğiniz proje dosyasının içerisine girin.

```sh
$ npm install
```
ile simple projesinin başlangıcı için gerekli olan npm modüllerini kurmanız gerekiyor.

```sh
$ node install
```
komut ile kurulumu başlatabilirsiniz. Sonrasını JS kendisi halledecektir.
Bu aşamada terminal tarafında bir hata ile karşılaşırsanız geri dönüş yapmayı unutmayın :)