// Dependencies
var fs = require('fs');
var url = require('url');
var http = require('http');
//var exec = require('child_process').exec;
//var spawn = require('child_process').spawn;
var extract = require('extract-zip');
var exec = require('child-process-promise').exec;
var spawn = require('child-process-promise').spawn;
var chalk = require('chalk');
var inquirer = require('inquirer');

// App variables
var file_url = 'http://www.erdogan.gen.tr/uxquark.zip';

var sorgu = [{
        type: "list",
        name: "os",
        message: "İşletim Sistemi Hangisi ?",
        choices: [
            {name: "Windows"},
            {name: "OSX"}
        ]
    }
]

inquirer.prompt(sorgu).then(function (answers) {
    var result = answers

    if(result.os === 'Windows'){
		var DOWNLOAD_DIR = '';
		
		 var child = exec('', function(err, stdout, stderr) {
            download_file_httpget(file_url);
        });
    } else {
        var DOWNLOAD_DIR = './';
		
        var mkdir = 'mkdir -p ' + DOWNLOAD_DIR;
        var child = exec(mkdir, function(err, stdout, stderr) {
            if (err) throw err;
            else download_file_httpget(file_url);
        });
    }    

    // Function to download file using HTTP.get
    var download_file_httpget = function(file_url) {
        var options = {
            host: url.parse(file_url).host,
            port: 80,
            path: url.parse(file_url).pathname
        };

        var file_name = url.parse(file_url).pathname.split('/').pop();
        var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

        console.log(chalk.white.bgRed.bold('   KURULUM BAŞLIYOR. LÜTFEN BEKLEYİNİZ...   '));        

        http.get(options, function(res) {
            res.on('data', function(data) {
                    file.write(data);
                }).on('end', function() {
                    file.end();
                    console.log(chalk.red.bold('UX Simple Project' + chalk.white.bold(' download ediliyor...')));

                    extract('./uxquark.zip', {dir: DOWNLOAD_DIR}, function (err) {
                        console.log(chalk.white.bold(chalk.green('[SUCCESS]') + ' > ' + chalk.cyan.bold('UX Simple Project ') + __dirname + ' Klasörüne extract edildi..'));
                        console.log(chalk.white.bold(chalk.yellow('[INFO]') + ' > ' + chalk.cyan.bold('NPM') + ' Dosyaları Kuruluyor...'));
                    });

                    exec('npm install').then(function (result) {
                        var stdout = result.stdout;
                        var stderr = result.stderr;
                        console.log('stdout: ', stdout);
                        console.log('stderr: ', stderr);

                        if(result){
                            console.log(chalk.white.bold(chalk.green('[SUCCESS]') + '> ' + chalk.cyan.bold('NPM') + ' Kurulumu Tamamlandı!'));
                            exec('bower-installer').then(function (result) {
                                console.log(chalk.white.bold(chalk.yellow('BOWER') + ' Dosyaları Kuruluyor...'));

                                if(result){
                                    console.log(chalk.white.bold(chalk.green('[SUCCESS]') + '> ' + chalk.cyan.bold('BOWER') + ' Kurulumu Tamamlandı!'));  

                                    exec('grunt build').then(function (result) {
                                        console.log(chalk.white.bold(chalk.yellow('[GRUNT]') + ' Compile ediliyor..'));    

                                        if(result){
                                            console.log(chalk.white.bold(chalk.yellow('[GRUNT]') + ' Tamamlandı!')); 

                                            console.log(chalk.italic.white.bold(''
                                                + 'UX Simple Project kurulumu başarılı şekilde tamamlandı.'
                                                + '\n\n'
                                                + 'Proje içerisinde kullanılacak komutlar ve yapı hakkında dökümantasyon hazırlanacaktır.'));  
                                        }
                                    }).catch(function (err) {
                                        console.error(chalk.red.bold('[GRUNT ERROR] : '), err);
                                    });    
                                }
                            }).catch(function (err) {
                                console.error(chalk.red.bold('[BOWER ERROR] : '), err);
                            });      
                        }
                    }).catch(function (err) {
                        console.error(chalk.red.bold('[NPM ERROR] : '), err);
                    });
                });
            }).on('error', function(e) {
              console.log("Download Error: " + e.message);
            });
    };
});